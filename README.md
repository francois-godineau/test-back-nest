<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo-small.svg" width="200" alt="Nest Logo" /></a>
</p>

[circleci-image]: https://img.shields.io/circleci/build/github/nestjs/nest/master?token=abc123def456
[circleci-url]: https://circleci.com/gh/nestjs/nest

  <p align="center">A progressive <a href="http://nodejs.org" target="_blank">Node.js</a> framework for building efficient and scalable server-side applications.</p>
    <p align="center">

## Description

This repository aims to gather all the backend technical tests that I had to do for job interviews.

[Nest](https://github.com/nestjs/nest) framework TypeScript starter repository.

## Jolimoi
- [Jolimoi](https://www.jolimoi.com/) technical test, 3h
- [Job offer](https://www.welcometothejungle.com/fr/companies/jolimoi/jobs/dev-front-javascript_paris) Lead dev fullstack
- [Test instructions](./doc)

### attack plan
- init back repo, bootstrap nest, readme : 30'
- init front repo, bootstrap svelte, readme : 30'
- implement step1 back + unit test : 30'
- implement step1 front + unit test : 30'
- implement step2 back + unit test : 30'
- implement step2 front + unit test : 30'
Total : 3h

Tool to enforce time management : [Pomodoro](https://pomofocus.io/app)

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov

# only one test
$ npm run test -- src/app.controller.spec.ts
```

## Openapi / Swagger
This app provides openapi endpoints. To view endpoints go to []
- Swagger Doc - [https://francois-godineau.herokuapp.com/api](https://francois-godineau.herokuapp.com/api)
- Swagger JSON - [https://francois-godineau.herokuapp.com/api-json](https://francois-godineau.herokuapp.com/api-json)

## Stay in touch

- Author - [Francois Godineau](https://www.linkedin.com/in/francois-godineau-76892225/)
- Svelte Front - [https://francois-godineau.web.app/](https://francois-godineau.web.app/)

## License

Nest is [MIT licensed](LICENSE).


## magic login strategy
vu là https://medium.com/@awaisshaikh94/implementing-passwordless-login-using-nestjs-4e6ef86b5269
le gros a tout piqué ici :
https://www.youtube.com/watch?v=69QmlAXNgHk&ab_channel=MariusEspejo

// For Sending Magic Link on Email:
http://localhost:3000/auth/login?email=developerorium@gmail.com

// For Validating Email (click on the Link you have received on your email)
https://localhost:3000/login?token=YOUR_TOKEN

## faire tourner un seul test
```
npm run test -- src/app.controller.spec.ts
```


## upgrade & audit
```bash
npx npm-check-updates -u
npm install
# ou
npm install --legacy-peer-deps
```

```bash
npm audit
npm audit fix
```
utiliser chatGPT quand trop compliqué pour avoir de l'aide

## docker
files : 
- `docker-compose.yml` : la stack utile au backend (un serveur mongo)
- `docker-compose-fullstack.yml` : le server + la stack utile => permet de tester l'image en local

```
docker-compose up -d -f docker-compose-server.yml
```

### create secret (local)


### create secret (portainer)