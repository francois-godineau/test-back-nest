FROM node:22.5-alpine
WORKDIR /usr/src/app
COPY package*.json ./
COPY tsconfig.build.json ./
RUN npm install 
COPY . .
RUN npm run build
EXPOSE 3000
CMD ["node", "dist/main"]
