import { Test, TestingModule } from '@nestjs/testing';
import { AuthController } from '@/auth/auth.controller';
import { UsersModule } from '@/users/users.module';
import { AuthGuard, PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { AuthService } from '@/auth/auth.service';
import { MagicLoginStrategy } from '@/auth/magiclogin.strategy';
import { JwtStrategy } from '@/auth/jwt.strategy';
import { ConfigModule } from '@nestjs/config';
import { MailModule } from '@/mail/mail.module';

describe('AuthController', () => {
  let controller: AuthController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        ConfigModule.forRoot({
          envFilePath: '.env.test',
          isGlobal: true,
        }),
        UsersModule,
        MailModule,
        PassportModule,
        JwtModule.register({
          secret: process.env.JWT_SECRET,
          signOptions: {
            expiresIn: '1h',
          },
        }),
      ],
      controllers: [AuthController],
      providers: [AuthService, MagicLoginStrategy, JwtStrategy],
    }).compile();

    controller = module.get<AuthController>(AuthController);
  });

  it('should call authService.findOrCreateUser and strategy.send when login is called', async () => {
    const req = {};
    const res = {};
    const body = { destination: 'test@example.com' };

    const authServiceSpy = jest
      .spyOn(controller['authService'], 'findOrCreateUser')
      .mockReturnValueOnce(null);
    const magicLoginSpy = jest
      .spyOn(controller['strategy'], 'send')
      .mockImplementationOnce(() => Promise.resolve());

    await controller.login(req, res, body);

    expect(authServiceSpy).toHaveBeenCalledWith(body.destination);
    expect(magicLoginSpy).toHaveBeenCalledWith(req, res);
  });

  it('should return tokens in callback', async () => {
    const req = { user: { id: '123', email: 'test@example.com' } };
    const tokens = { access_token: 'token' };

    jest
      .spyOn(controller['authService'], 'generateTokens')
      .mockReturnValueOnce(tokens);

    const result = await controller.callback(req);

    expect(result).toEqual(tokens);
  });

  it('should use AuthGuard with magiclogin strategy in callback', () => {
    const guards = Reflect.getMetadata('__guards__', controller.callback);
    const guard = guards[0];

    expect(guard).toBe(AuthGuard('magiclogin'));
  });
});
