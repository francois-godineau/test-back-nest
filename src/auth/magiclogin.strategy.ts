import { Injectable, Logger } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import Strategy from 'passport-magic-login';
import { AuthService } from '@/auth/auth.service';
import { MailService } from '@/mail/mail.service';

@Injectable()
export class MagicLoginStrategy extends PassportStrategy(Strategy) {
  private readonly logger = new Logger(MagicLoginStrategy.name);

  constructor(
    private authService: AuthService,
    private mailService: MailService,
  ) {
    super({
      secret: process.env.MAGIC_LOGIN_SECRET,
      jwtOptions: {
        expiresIn: '5m',
      },
      callbackUrl: `${process.env.BASE_URL}/auth/login/callback`,
      sendMagicLink: async (destination, href) => {
        await this.mailService.sendEmail(destination, 'your access link', href);

        this.logger.debug(`sending email to ${destination} with Link ${href}`);
      },
      verify: async (payload, callback) =>
        callback(null, this.validate(payload)),
    });
  }

  // validate makes user available in req.user
  validate(payload: { destination: string }) {
    const user = this.authService.validateUser(payload.destination);

    return user;
  }
}
