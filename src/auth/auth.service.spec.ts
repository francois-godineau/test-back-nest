import { Test, TestingModule } from '@nestjs/testing';
import { AuthService } from '@/auth/auth.service';
import { UsersService } from '@/users/users.service';
import { JwtService } from '@nestjs/jwt';
import { UnauthorizedException } from '@nestjs/common';
import { User } from '@/users/entities/user.entity';
import { ConfigModule } from '@nestjs/config';

describe('AuthService', () => {
  let authService: AuthService;
  let usersService: UsersService;
  let jwtService: JwtService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        ConfigModule.forRoot({
          envFilePath: '.env.test',
          isGlobal: true,
        }),
      ],
      providers: [
        AuthService,
        {
          provide: UsersService,
          useValue: {
            findOneByEmail: jest.fn(),
            create: jest.fn(),
          },
        },
        {
          provide: JwtService,
          useValue: {
            sign: jest.fn(),
          },
        },
      ],
    }).compile();

    authService = module.get<AuthService>(AuthService);
    usersService = module.get<UsersService>(UsersService);
    jwtService = module.get<JwtService>(JwtService);
  });

  // Test pour findOrCreateUser
  it('should findOrCreateUser a new user if email is not found', async () => {
    const email = 'test@example.com';
    const newUser = { id: '1', email } as User;

    jest.spyOn(usersService, 'findOneByEmail').mockResolvedValueOnce(null);
    jest.spyOn(usersService, 'create').mockResolvedValueOnce(newUser);

    const result = await authService.findOrCreateUser(email);

    expect(usersService.findOneByEmail).toHaveBeenCalledWith(email);
    expect(usersService.create).toHaveBeenCalledWith({ email });
    expect(result).toEqual(newUser);
  });

  it('should return the user if email already exists', async () => {
    const email = 'test@example.com';
    const existingUser = { id: '1', email } as User;

    jest
      .spyOn(usersService, 'findOneByEmail')
      .mockResolvedValueOnce(existingUser);

    const result = await authService.findOrCreateUser(email);

    expect(usersService.findOneByEmail).toHaveBeenCalledWith(email);
    expect(usersService.create).not.toHaveBeenCalled();
    expect(result).toEqual(existingUser);
  });

  // Test pour validateUser
  it('should validate the user if email exists', async () => {
    const email = 'test@example.com';
    const existingUser = { id: '1', email } as User;

    jest
      .spyOn(usersService, 'findOneByEmail')
      .mockResolvedValueOnce(existingUser);

    const result = await authService.validateUser(email);

    expect(usersService.findOneByEmail).toHaveBeenCalledWith(email);
    expect(result).toEqual(existingUser);
  });

  it('should throw UnauthorizedException if email is not found', async () => {
    const email = 'test@example.com';

    jest.spyOn(usersService, 'findOneByEmail').mockResolvedValueOnce(null);

    await expect(authService.validateUser(email)).rejects.toThrow(
      UnauthorizedException,
    );
  });

  // Test pour generateTokens
  it('should generate tokens for a valid user', () => {
    const user = { id: '1', email: 'test@example.com' } as User;
    const token = 'mocked_token';
    const payload = { sub: user.id, email: user.email };
    const secret = 'mocked_jwt_secret';
    process.env.JWT_SECRET = secret;

    jest.spyOn(jwtService, 'sign').mockReturnValue(token);

    const result = authService.generateTokens(user);
    console.log('--> result', result)
    expect(jwtService.sign).toHaveBeenCalledWith(payload, { secret });
    expect(result).toEqual({ access_token: token });
  });

  it('should generate tokens with default user if user is undefined', () => {
    const token = 'mocked_token';
    const defaultUser = { id: '1', email: 'test@mail.com' } as User;
    const payload = { sub: defaultUser.id, email: defaultUser.email };
    const secret = 'mocked_jwt_secret';
    process.env.JWT_SECRET = secret;
  
    jest.spyOn(jwtService, 'sign').mockReturnValue(token);
  
    const result = authService.generateTokens(undefined);
  
    expect(jwtService.sign).toHaveBeenCalledWith(payload, { secret });
    expect(result).toEqual({ access_token: token });
  });
  
});
