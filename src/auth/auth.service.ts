import { Injectable, UnauthorizedException } from '@nestjs/common';
import { UsersService } from '@/users/users.service';
import { User } from '@/users/entities/user.entity';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  async findOrCreateUser(email: string) {
    let user = await this.usersService.findOneByEmail(email);
    if (!user) {
      user = await this.usersService.create({ email });
    }

    return user;
  }

  async validateUser(email: string) {
    const user = await this.usersService.findOneByEmail(email);
    if (!user) {
      throw new UnauthorizedException();
    }

    return user;
  }

  generateTokens(user: User) {
    if (!user) user = { id: '1', email: 'test@mail.com' };
    const payload = { sub: user.id, email: user.email };
    return {
      access_token: this.jwtService.sign(payload, {
        secret: process.env.JWT_SECRET,
      }),
    };
  }
}
