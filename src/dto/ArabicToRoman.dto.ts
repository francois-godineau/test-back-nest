import { IsNumber, Min, Max } from 'class-validator';

export class ArabicToRomanDto {
  @IsNumber()
  @Min(0)
  @Max(100)
  decimal: number;
}
