import { Test, TestingModule } from '@nestjs/testing';
import { UsersController } from '@/users/users.controller';
import { UsersService } from '@/users/users.service';
import { CreateUserDto } from '@/users/dto/create-user.dto';
import { UpdateUserDto } from '@/users/dto/update-user.dto';

describe('UsersController', () => {
  let controller: UsersController;
  let usersService: UsersService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UsersController],
      providers: [
        {
          provide: UsersService,
          useValue: {
            create: jest.fn(),
            findAll: jest.fn(),
            findOne: jest.fn(),
            update: jest.fn(),
            remove: jest.fn(),
          },
        },
      ],
    }).compile();

    controller = module.get<UsersController>(UsersController);
    usersService = module.get<UsersService>(UsersService);
  });

  // Test pour create
  it('should call usersService.create with CreateUserDto', async () => {
    const createUserDto: CreateUserDto = {
      email: 'test@example.com',
    };
    const result = { id: '1', ...createUserDto };

    jest.spyOn(usersService, 'create').mockResolvedValue(result);

    expect(await controller.create(createUserDto)).toEqual(result);
    expect(usersService.create).toHaveBeenCalledWith(createUserDto);
  });

  // Test pour findAll
  it('should return an array of users', async () => {
    const result = [{ id: '0', email: 'test@example.com' }];

    jest.spyOn(usersService, 'findAll').mockResolvedValue(result);

    expect(await controller.findAll()).toEqual(result);
    expect(usersService.findAll).toHaveBeenCalled();
  });

  // Test pour findOne
  it('should return a single user by id', async () => {
    const id = '0';
    const result = { id, email: 'test@example.com' };

    jest.spyOn(usersService, 'findOne').mockResolvedValue(result);

    expect(await controller.findOne(id)).toEqual(result);
    expect(usersService.findOne).toHaveBeenCalledWith(id);
  });

  // Test pour update
  // it('should update a user by id', async () => {
  //   const id = '1';
  //   const updateUserDto: UpdateUserDto = { id, email: 'updated@example.com' };
  //   const result = { id, ...updateUserDto };

  //   jest.spyOn(usersService, 'update').mockResolvedValue(result);

  //   expect(await controller.update(id, updateUserDto)).toEqual(result);
  //   expect(usersService.update).toHaveBeenCalledWith(id, updateUserDto);
  // });

  // Test pour remove
  // it('should remove a user by id', async () => {
  //   const id = '1';
  //   const result = { deleted: true };

  //   jest.spyOn(usersService, 'remove').mockResolvedValue(result);

  //   expect(await controller.remove(id)).toEqual(result);
  //   expect(usersService.remove).toHaveBeenCalledWith(id);
  // });
});
