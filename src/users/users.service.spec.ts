import { Test, TestingModule } from '@nestjs/testing';
import { UsersService } from '@/users/users.service';
import { CreateUserDto } from '@/users/dto/create-user.dto';
import { UpdateUserDto } from '@/users/dto/update-user.dto';
import { User } from '@/users/entities/user.entity';

describe('UsersService', () => {
  let service: UsersService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [UsersService],
    }).compile();

    service = module.get<UsersService>(UsersService);
  });

  // Test pour create
  it('should create a new user', async () => {
    const createUserDto: CreateUserDto = { email: 'new@mail.com' };
    const result = await service.create(createUserDto);

    expect(result).toEqual({
      id: '4', // Vu que la taille initiale de `users` est 3
      email: 'new@mail.com',
    });
    expect(service.findAll()).resolves.toHaveLength(4);
  });

  // Test pour findAll
  it('should return all users', async () => {
    const result = await service.findAll();
    expect(result).toHaveLength(3); // La taille initiale de `users` est 3
    expect(result).toEqual(service['users']);
  });

  // Test pour findOne
  it('should return a user by id', async () => {
    const result = await service.findOne('1');
    expect(result).toEqual({
      id: '1',
      email: 'test-1@mail.com',
      name: 'test-1',
    });
  });

  it('should return undefined if user is not found by id', async () => {
    const result = await service.findOne('non-existing-id');
    expect(result).toBeUndefined();
  });

  // Test pour findOneByEmail
  it('should return a user by email', async () => {
    const result = await service.findOneByEmail('test-1@mail.com');
    expect(result).toEqual({
      id: '1',
      email: 'test-1@mail.com',
      name: 'test-1',
    });
  });

  it('should return undefined if user is not found by email', async () => {
    const result = await service.findOneByEmail('non-existing@mail.com');
    expect(result).toBeUndefined();
  });

  // Test pour update
  it('should update a user by id', async () => {
    const updateUserDto: UpdateUserDto = { id: '1', email: 'updated@mail.com', name: 'updated name' };
    const result = await service.update('1', updateUserDto);

    expect(result).toEqual('This action updates a #1 user');
  });

  // Test pour remove
  it('should remove a user by id', async () => {
    const result = await service.remove('1');

    expect(result).toEqual('This action removes a #1 user');
  });
});
