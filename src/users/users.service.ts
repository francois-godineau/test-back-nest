import { Injectable } from '@nestjs/common';
import { CreateUserDto } from '@/users/dto/create-user.dto';
import { UpdateUserDto } from '@/users/dto/update-user.dto';
import { User } from '@/users/entities/user.entity';

@Injectable()
export class UsersService {
  private readonly users: User[] = [
    { id: '0', email: 'test@mail.com', name: 'test' },
    { id: '1', email: 'test-1@mail.com', name: 'test-1' },
    { id: '2', email: 'test-2@mail.com', name: 'test-2' },
  ];
  async create(createUserDto: CreateUserDto) {
    const user = {
      id: (this.users.length + 1).toString(),
      ...createUserDto,
    };
    this.users.push(user);
    return user;
  }

  async findAll() {
    return this.users;
  }

  async findOne(id: string) {
    return this.users.find((user) => user.id === id);
  }

  async findOneByEmail(email: string) {
    return this.users.find((user) => user.email === email);
    // return `This action returns a #${email} user`;
  }

  async update(id: string, updateUserDto: UpdateUserDto) {
    return `This action updates a #${id} user`;
  }

  async remove(id: string) {
    return `This action removes a #${id} user`;
  }
}
