import { Test, TestingModule } from '@nestjs/testing';
import { AppController } from '@/app.controller';
import { AppService } from '@/app.service';
import { SseService } from '@/services/sse.service';
import { ArabicToRomanDto } from '@/dto/ArabicToRoman.dto';
import { JwtModule } from '@nestjs/jwt';
import { AuthModule } from '@/auth/auth.module';
import { UsersModule } from '@/users/users.module';
import { ConfigModule } from '@nestjs/config';

describe('AppController', () => {
  let appController: AppController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      imports: [
        AuthModule,
        UsersModule,
        ConfigModule.forRoot({
          envFilePath: '.env.test',
          isGlobal: true,
        }),
        JwtModule.register({ secret: process.env.JWT_SECRET }),
      ],
      controllers: [AppController],
      providers: [AppService, SseService],
    }).compile();

    appController = app.get<AppController>(AppController);
  });

  describe('root', () => {
    it('should return "Hello World!"', () => {
      expect(appController.getHello()).toBe('Hello World!');
    });
  });

  describe('# jolimoi', () => {
    it('should convert arabic to roman', () => {
      const tests = {
        1: 'I',
        4: 'IV',
        9: 'IX',
        11: 'XI',
        27: 'XXVII',
        49: 'XLIX',
        52: 'LII',
      };
      Object.keys(tests).forEach((decimal) => {
        const dto = new ArabicToRomanDto();
        dto.decimal = parseInt(decimal);
        const res = appController.getJolimoiArabicToRoman(dto);
        expect(res).toBe(tests[decimal]);
      });
    });
  });

  it('should return "Welcome, test@mail.com!" for protected route', () => {
    const result = appController.protectedRoute({
      user: { name: 'test@mail.com' },
    });
    expect(result).toBe('Welcome, test@mail.com!');
  });
});
