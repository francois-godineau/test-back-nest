import { Injectable } from '@nestjs/common';
import Mailgun from 'mailgun.js';
import * as FormData from 'form-data';

@Injectable()
export class MailService {
  private mailgun: any;
  private domain: string;

  constructor() {
    const mailgun = new Mailgun(FormData);
    this.mailgun = mailgun.client({
      username: 'api',
      key: process.env.MAILGUN_KEY,
    });
    this.domain = process.env.MAILGUN_DOMAIN;
  }

  async sendEmail(to: string, subject: string, text: string): Promise<void> {
    try {
      const message = {
        from: `L'Artiste Crypto <no-reply@${this.domain}>`,
        to,
        subject,
        text,
      };
      await this.mailgun.messages.create(this.domain, message);
    } catch (error) {
      console.error('Error sending email:', error);
    }
  }
}
