import { Controller, Get, Post, Body, Sse, UseGuards, Req } from '@nestjs/common';
import { Observable } from 'rxjs';
import { AppService } from '@/app.service';
import { ArabicToRomanDto } from '@/dto/ArabicToRoman.dto';
import { SseService } from '@/services/sse.service';
import { AuthGuard } from '@nestjs/passport';

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    private readonly sseService: SseService,
  ) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @Post('/api/jolimoi/arabic-to-roman')
  getJolimoiArabicToRoman(@Body() arabicToRomanDto: ArabicToRomanDto): string {
    const result = this.appService.getJolimoiArabicToRoman(arabicToRomanDto);
    this.sseService.addEvent({ data: result });
    return result;
  }

  @Sse('/api/jolimoi/sse')
  sse(): Observable<any> {
    return this.sseService.sendEvents();
  }

  @Get('/protected-route')
  @UseGuards(AuthGuard('jwt'))
  protectedRoute(@Req() req) {
    // Access authorized user information from the request using `@Req()` decorator
    return `Welcome, ${req.user.name}!`;
  }
}
