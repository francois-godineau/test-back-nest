import { Injectable } from '@nestjs/common';
import { ArabicToRomanDto } from '@/dto/ArabicToRoman.dto';
import { toRoman } from 'ronu';

@Injectable()
export class AppService {
  getHello(): string {
    return 'Hello World!';
  }

  getJolimoiArabicToRoman(arabicToRomanDto: ArabicToRomanDto): string {
    return toRoman(arabicToRomanDto.decimal);
  }
}
